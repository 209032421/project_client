/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package the.bank;

/**
 *
 * @author M.Ihsaan
 */
public class Bank {
     private int branchCode;
    private String address;
    private String telephone;
    
    //Setters
    public void setBranchCode(int branchCode) {
        this.branchCode = branchCode;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    //Getters
    public int getBranchCode() {
        return branchCode;
    }

    public String getAddress() {
        return address;
    }

    public String getTelephone() {
        return telephone;
    }

    private Bank() {
    }
    
    public Bank(int branchCode, String address, String telephone) {
        this.branchCode = branchCode;
        this.address = address;
        this.telephone = telephone;
    }
    
}
