/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package the.bank;

/**
 *
 * @author M.Ihsaan
 */
public class Customer {
    private int customerNumber;
    private String name;
    private String surname;
    private String contactNumber;
    private int accountNumber;
    
    //getters
    public int getCustomerNumber() {
        return customerNumber;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public int getAccountNumber() {
        return accountNumber;
    }
    
      //Setters
    public void setCustomerNumber(int customerNumber) {
        this.customerNumber = customerNumber;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public void setAccountNumber(int accountNumber) {
        this.accountNumber = accountNumber;
    }
    
    public Customer() {
    }
    
    public Customer(int customerNumber ,String name, String surname, String contactNumber /*int accountNumber*/) {
        this.customerNumber=customerNumber;
        this.name = name;
        this.surname = surname;
        this.contactNumber = contactNumber;
        //this.accountNumber = accountNumber;
        this.customerNumber=customerNumber;
    }
    
}
