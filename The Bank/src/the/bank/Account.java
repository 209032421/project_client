/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package the.bank;

/**
 *
 * @author M.Ihsaan
 */
public class Account {
    private int accountNumber;
    private double balance;
    private String accountType;
    
    
    //Constructor
    private Account(){}
    
    public Account(int accountNumber, double balance, String accountType) {
        this.balance = balance;
        this.accountType = accountType;
        this.accountNumber = accountNumber;
    }
    
    //Getters
    public int getAccountNumber() {return accountNumber;}

    public double getBalance() {return balance;}

    public String getAccountType() {return accountType;}
        
   //Setters
    public void setAccountNumber(int accountNumber) {
        this.accountNumber = accountNumber;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }
    
}
